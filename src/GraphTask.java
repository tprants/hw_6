import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
      //throw new RuntimeException ("Nothing implemented yet!"); // delete this
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      //Graph a = new Graph ("A");
      
      g.createRandomSimpleGraph (6, 9);
      //g.createRandomSimpleGraph(3, 3);
      System.out.println (g);
      System.out.println (g.first);
      //System.out.println (g.first.info);
      //System.out.println(g.info);
      //a.deleteVertex(g.first);
      //g.deleteVertex(new Vertex("n2"));
      
      /*System.out.println (g);*/
      System.out.println(g.first.next);
      g.deleteVertex(g.first.next);
      System.out.println (g);
      /*System.out.println(g.first.next.next);
      g.deleteVertex(g.first.next.next);
      System.out.println (g);
      System.out.println(g.first.next.next.next);
      g.deleteVertex(g.first.next.next.next);
      System.out.println (g);
      System.out.println ("S�stemaatne kustutamine");*/
      
      /*g.createRandomSimpleGraph (6, 9);
      System.out.println (g);
      while(g.first!=null) {
    	  System.out.println(g.first);
          g.deleteVertex(g.first);
          System.out.println (g);
      }*/
      //System.out.println(g.info);

   }

   // TODO!!! add javadoc relevant to your problem
   /** The problem assigned to me was to make a function that can remove a vertex from the graph
    * with all the arcs from and to it aswell.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      // TODO!!! Your Graph methods here! Probably your solution belongs here.
      
      /**
       * Takes a Vertex type object and removes it from the graph, also removes all all arcs to and from the vertex
       * if the graph is empty, the input is null or the vertex doesn't exist in the graph, the function gives
       * a Runtime Exception
       * @param deletable Vertex that will be deleted from the graph
       */
      public void deleteVertex(Vertex deletable) {
    	  //erandid
    	  if(deletable == null) {
    		  throw new RuntimeException("Input is null");
    	  }
    	  if(this.first == null) {
    		  throw new RuntimeException("The graph is empty");
    	  }
    	  boolean kasTippOli = false;
    	  //tipude �lesseadmine
    	  Vertex lastV = first;
          Vertex v = first;
          //kaarte �lesseadmine
          Arc lastA = v.first;
          Arc a = v.first;
          while (v != null) {
        	  //kui hetkel vaadeldav tipp on kustutatav
        	 if(v.equals(deletable)) {
        		 kasTippOli=true;
        		 this.info--;
        		 //kui esimene vaadeldav tipp on kustutatav
        		 if(lastV.equals(v)) {
        			 this.first=v.next;
        		 }
        		 //kui m�ni j�rgnev tipp on kustutatav (kontrolli kas t��tab ka viimase tipuga)
        		 else {
        			 lastV.next=v.next;
        		}
        	 }
        	 else {
        		 lastA = v.first;
        		 a = v.first;
                 while (a != null) {
                	 //kui vaadeldava kaare siht on kustutatav tipp
                	 if(a.target.equals(deletable)) {
                		 //kui tegu on esimese kaarega nimekirjas
                		 if(lastA.equals(a)) {
                			 v.first=a.next;
                		 }
                		 //kui tegu on m�ne mitte esimese kaarega nimekirjas
                		 else {
                			 lastA.next=a.next;
                		 }
                	 }
                	 lastA =a;
                     a = lastA.next;
                 }
        	 }
        	 lastV = v;
             v = lastV.next;
          }
          if(!kasTippOli) {
        	  throw new RuntimeException("The graph dosent have this vertex: "+deletable);
          }
      }
   }

} 

